/* eslint-disable no-empty-function */
/* eslint-disable class-methods-use-this */
const jwt = require('jsonwebtoken');
const { Collection, Card, Tag } = require('../models/models');
const ApiError = require('../error/ApiError');

class CollectionController {
  async create(req, res) {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      const { id } = jwt.verify(token, process.env.SECRET_KEY);
      const { title } = req.body;
      const collection = await Collection.create({ title, userId: id });
      return res.json(collection);
    }
    return res.status(401).json({ message: 'Пользователь не авторизован' });
  }

  async getAll(req, res) {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      const { id } = jwt.verify(token, process.env.SECRET_KEY);
      const collections = await Collection.findAll({ where: { userId: id } });
      return res.json({ collections });
    }
    return res.status(401).json({ message: 'Пользователь не авторизован' });
  }

  async getAllWithCards(req, res) {
    if (req.headers.authorization) {
      const token = req.headers.authorization.split(' ')[1];
      const { id: userId } = jwt.verify(token, process.env.SECRET_KEY);
      const collectionsWithCards = await Collection.findAll({
        where: { userId },
        include: [{
          model: Card,
          as: 'cards',
        }],
      });
      return res.json({ collectionsWithCards });
    }
    return res.status(401).json({ message: 'Пользователь не авторизован' });
  }

  async getOne(req, res) {
    const { id } = req.params;
    const collection = await Collection.findOne({ where: { id } });
    res.json(collection);
  }

  async update(req, res) {
    const { title, id } = req.body;
    const updated = await Collection.update({ title }, { where: { id }, returning: true });
    res.json(...updated[1]);
  }

  async delete(req, res) {
    const { id } = req.params;
    const deletedRows = await Collection.destroy({ where: { id } });
    res.json(deletedRows);
  }
}
module.exports = new CollectionController();
