/* eslint-disable max-len */
/* eslint-disable quotes */
/* eslint-disable prefer-template */
/* eslint-disable consistent-return */
/* eslint-disable no-empty-function */
/* eslint-disable class-methods-use-this */
const uuid = require('uuid');
const path = require('path');
const ogs = require('open-graph-scraper');
const { Card } = require('../models/models');
const ApiError = require('../error/ApiError');

class CardController {
  async create(req, res, next) {
    try {
      const {
        url,
        tagId,
        collectionId,
      } = req.body;

      const options = { url };
      const { result } = await ogs(options);
      const { ogTitle, ogUrl, ogDescription } = result;

      const card = await Card.create({
        header: ogTitle, img: result.ogImage.url, content: ogDescription, url: ogUrl, tagId, collectionId,
      });

      return res.json(card);
    } catch (error) {
      next(ApiError.badRequest(error.message));
    }
  }

  async getAll(req, res) {
    const cards = await Card.findAll();
    return res.json({ cards });
  }

  async getOne(req, res) {
    const { id } = req.params;
    const cards = await Card.findAll({ where: { id } });
    return res.json({ cards });
  }

  async update(req, res) {
    const {
      id, header, content, url,
    } = req.body;
    const updated = await Card.update({ header, content, url }, { where: { id }, returning: true });
    res.json(...updated[1]);
  }

  async delete(req, res) {
    const { id } = req.params;
    const deletedRows = await Card.destroy({ where: { id } });
    res.json(deletedRows);
  }
}
module.exports = new CardController();
