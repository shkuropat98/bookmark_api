const Router = require('express');

const userRouter = require('./userRouter');
const collectionRouter = require('./collectionRouter');
const cardRouter = require('./cardRouter');
const tagRouter = require('./tagRouter');

const router = new Router();

router.use('/user', userRouter);
router.use('/collection', collectionRouter);
router.use('/card', cardRouter);
router.use('/tag', tagRouter);

module.exports = router;
